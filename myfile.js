const test = require('selenium-webdriver/testing');
const webdriver = require('selenium-webdriver');
const { expect } = require('chai');
const webdriverUntil = require('selenium-webdriver/lib/until')

const driver = new webdriver.Builder().
withCapabilities(webdriver.Capabilities.chrome()).
build();

const waitFor = (selector) => {
  driver.wait(webdriverUntil.elementLocated(webdriver.By.css(selector)), 1000)
}

const navigate = (url) => {
  driver.get(url);
}

const selectElement = (selector) => {
  waitFor(selector);
  return driver.findElement(webdriver.By.css(selector))
}

const TEXT = 'hello world';
const URL = 'http://localhost:8888';

describe('Textbox counter', function () {

  this.timeout(15000)
  beforeEach(() => {
    navigate(URL);
  });

  test.it('can write', () => {
    let text = selectElement("#myText");
    text.sendKeys(TEXT);
    text.getAttribute('value').then((value) => {
      expect(value).to.be.equal(TEXT);
    });
  });

  test.it('can count', () => {
    let text = selectElement("#myText");
    text.sendKeys(TEXT);

    let counter = selectElement("#total");
    counter.getText().then((value) => {
      expect(TEXT.length).to.be.equal(parseInt(value));
    });

    driver.quit();

  });
});
