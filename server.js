const connect = require("connect");
const serveStatic = require('serve-static');
const PORT = 8888;
connect().use(serveStatic(__dirname)).listen(PORT); // Mucho mejor 😉
console.log("Servidor corriendo  en puerto http://localhost:" + PORT);
